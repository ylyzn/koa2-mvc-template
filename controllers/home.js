module.exports = ({ $services }) => {
  return {
    index: async (ctx, next) => {
      // TODO
      const { title, content } = await $services.home.index()
      await ctx.render('index', { title, content })
    },
  }
}
