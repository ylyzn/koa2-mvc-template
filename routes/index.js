module.exports = ({ router, $controllers }) => {
  // TODO
  router.get('/', $controllers.home.index)

  router.get('/string', async (ctx, next) => {
    ctx.body = 'koa2 string'
  })

  router.get('/json', async (ctx, next) => {
    ctx.body = {
      title: 'koa2 json',
    }
  })
  return router
}
